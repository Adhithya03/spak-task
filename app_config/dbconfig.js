var mysql = require('mysql')
const dotenv = require('dotenv')
dotenv.config()
console.log(`Your port is ${process.env.PORT}`)
process.env.SECRET_KEY = 'DWRGOgNaLr'
global.NODE_ENV = process.env.NODE_ENV
console.log(global.NODE_ENV)
const mysqlConfig = require('../helpers/mysql.json')
let pool = ''

if (global.NODE_ENV == 'DEV') {
  pool = mysql.createPool({
    connectionLimit: mysqlConfig.DEV.connectionLimit,
    host: mysqlConfig.DEV.host,
    user: mysqlConfig.DEV.user,
    password: mysqlConfig.DEV.password,
    database: mysqlConfig.DEV.database,
    debug: mysqlConfig.DEV.debug
  })
} else if (global.NODE_ENV == 'TEST') {
  pool = mysql.createPool({
    connectionLimit: mysqlConfig.TEST.connectionLimit,
    host: mysqlConfig.TEST.host,
    user: mysqlConfig.TEST.user,
    password: mysqlConfig.TEST.password,
    database: mysqlConfig.TEST.database,
    debug: mysqlConfig.TEST.debug
  })
} else if (global.NODE_ENV == 'PRODUCTION') {
  pool = mysql.createPool({
    connectionLimit: mysqlConfig.PRODUCTION.connectionLimit,
    host: mysqlConfig.PRODUCTION.host,
    user: mysqlConfig.PRODUCTION.user,
    password: mysqlConfig.PRODUCTION.password,
    database: mysqlConfig.PRODUCTION.database,
    debug: mysqlConfig.PRODUCTION.debug
  })
} else {
  pool = mysql.createPool({
    connectionLimit: mysqlConfig.LOCAL.connectionLimit,
    host: mysqlConfig.LOCAL.host,
    user: mysqlConfig.LOCAL.user,
    password: mysqlConfig.LOCAL.password,
    database: mysqlConfig.LOCAL.database,
    debug: mysqlConfig.LOCAL.debug
  })
}

function executeQuery(sql, val, callback) {
  pool.getConnection((err, connection) => {
    if (err) {
      return callback(err, null)
    } else {
      if (connection) {
        // val = connection.escape(val)
        connection.query(sql, val, function (error, results, fields) {
          connection.release()
          if (error) {
            return callback(error, null)
          }
          return callback(null, results)
        })
      }
    }
  })
}

function query(sql, val, callback) {
  executeQuery(sql, val, function (err, data) {
    if (err) {
      return callback(err)
    }
    callback(null, data)
  })
}

module.exports = {
  query: query
}
