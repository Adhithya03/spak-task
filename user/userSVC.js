const db = require("../app_config/dbconfig.js");

/**
 *  @function Register_User
 *
 *  @description Register user into user table.
 */

const Register_User = function (user_details) {
  const insertUser =
    "INSERT INTO user (UserID, name,contact,address,gender,country) VALUES ?";
  return new Promise(function (resolve, reject) {
    const userdetails = {
      name: user_details.name,
      contact: user_details.contact,
      address: user_details.address,
      gender: user_details.gender,
      country: user_details.country,
    };
    const userinfo = [
      [
        userdetails.name,
        userdetails.contact,
        userdetails.address,
        userdetails.gender,
        userdetails.country,
      ],
    ];
    db.query(insertUser, [userinfo], function (err, data) {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        resolve(data.insertId);
      }
    });
  });
};

/**
 *  @function Login_user
 *
 *  @description Login user into user table.
 */

const Login_user = function (user_details) {
  const CheckUser = "Select * from user  where name = ? AND contact = ? ";
  return new Promise(function (resolve, reject) {
    db.query(
      CheckUser,
      [user_details.name, user_details.contact],
      function (err, data) {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          resolve(data);
        }
      }
    );
  });
};

/**
 *  @function Search_user
 *
 *  @description Login user into user table.
 */

const Search_user = function (user_details) {
  const getAllUser = "Select * from user where name LIKE ";
  return new Promise(function (resolve, reject) {
    db.query(
      getAllUser +
        "'" +
        "%" +
        user_details.name +
        "%" +
        "'" +
        " AND contact LIKE '" +
        "%" +
        user_details.contact +
        "%" +
        "';",
      [],
      function (err, data) {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          resolve(data);
        }
      }
    );
  });
};

module.exports = {
  Register_User,
  Login_user,
  Search_user,
};
