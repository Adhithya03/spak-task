const express = require("express");
const router = express.Router();
const moment = require("moment");
const async = require("async");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");
const userSVC = require("./userSVC.js");
const HttpStatus = require("http-status-codes");

router.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

/**
 * @api {post} /register_user
 * @apiVersion V1
 * @apiName register_user
 * @apiGroup user
 *
 * @apiParam {user_details}
 * {
    name: '',
    contact: '',
    address: '',
    gender: '',
    country: '',
  }
 *
 * @apiDesc  Used to Register User into db.
 *
 * @apiSuccess {result}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
      status: ''
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Make a call to UserSVC to Register User into db.
 *   2. IF sucessfully send succressResponse.
 *   3. IF Erorr Send Error-Response.
 */

router.post("/register_user", function (req, res) {
  const user_details = req.body;
  let result = "";
  async.series(
    [
      // Step-1 Make a call to UserSVC to Register User into db.
      function (callback) {
        userSVC
          .Register_User(user_details)
          .then(function (data) {
            callback(null, 1);
          })
          .catch(function (e) {
            // Step-3 IF Erorr Send Error-Response.
            errorMessage = {
              success: false,
              message: "Internal Server error",
            };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
    ],
    function () {
      result = {
        status: "Success",
      };
      // Step-2 IF sucessfully send succressResponse
      res.status(HttpStatus.OK).send(result);
    }
  );
});

/**
 * @api {post} /login_user
 * @apiVersion V1
 * @apiName login_user
 * @apiGroup user
 *
 * @apiParam {user_details}
 * {
    name: '',
    contact: '',
  }
 *
 * @apiDesc  Used to User Login.
 *
 * @apiSuccess {result}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
      status: ''
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Make a call to UserSVC to User Login.
 *   2. Generate and JWT Token for User.
 *   3. IF sucessfully send succressResponse.
 *   4. IF Erorr Send Error-Response.
 */

router.post("/login_user", function (req, res) {
  const user_details = req.body;
  let result = "";
  let token = "";
  async.series(
    [
      // Step-1 Make a call to UserSVC to User Login.
      function (callback) {
        userSVC
          .Login_user(user_details)
          .then(function (data) {
            data.length == 0
              ? ((errorMessage = {
                  success: false,
                  message: "Name OR Contact is Incorrect",
                }),
                res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage))
              : callback(null, 1);
          })
          .catch(function (e) {
            // Step-4 IF Erorr Send Error-Response.
            errorMessage = {
              success: false,
              message: "Internal Server error",
            };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
      // Step-2 Generate and JWT Token for User.
      function (callback) {
        token = jwt.sign(
          {
            username: user_details.name,
          },
          process.env.SECRET_KEY,
          {
            expiresIn: "10d", // token expires in 10 Days
          }
        );
        callback(null, 2);
      },
    ],
    function () {
      result = {
        status: "Success",
      };
      // Step-3 IF sucessfully send succressResponse
      res.setHeader("token", token);
      res.status(HttpStatus.OK).send(result);
    }
  );
});

/**
 * @api {post} /search_user
 * @apiVersion V1
 * @apiName search_user
 * @apiGroup user
 *
 * @apiParam {user_details}
 * {
    name: '',
    contact: '',
  }
 *
 * @apiDesc  Used to Get All Users from db.
 *
 * @apiSuccess {result}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
      status: ''
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Check JWT Token For User.
 *   2. Make a call to UserSVC to Get All Users from db.
 *   3. IF sucessfully send succressResponse.
 *   4. IF Erorr Send Error-Response.
 */

router.post("/search_user", function (req, res) {
  const user_details = req.body;
  let allUsers = "";
  const userToken = req.headers.token;
  async.series(
    [
      // Step-1 Check JWT Token For User.
      function (callback) {
        jwt.verify(userToken, process.env.SECRET_KEY, function (err, decoded) {
          if (err) {
            // Step-4 IF Erorr Send Error-Response.
            errorMessage = {
              success: false,
              message: "User UnAuthorized Token",
            };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          } else {
            console.log(decoded.foo); // bar
            callback(null, 1);
          }
        });
      },
      // Step-2 Make a call to UserSVC to Get All Users from db.
      function (callback) {
        userSVC
          .Search_user(user_details)
          .then(function (data) {
            data.length == 0
              ? ((errorMessage = {
                  success: false,
                  message: "Name OR Contact is Incorrect",
                }),
                res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage))
              : ((allUsers = data), callback(null, 1));
          })
          .catch(function (e) {
            // Step-4 IF Erorr Send Error-Response.
            errorMessage = {
              success: false,
              message: "Internal Server error",
            };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          });
      },
    ],
    function () {
      // Step-3 IF sucessfully send succressResponse
      res.status(HttpStatus.OK).send(allUsers);
    }
  );
});

/**
 * @api {get} /logout
 * @apiVersion V1
 * @apiName logout
 * @apiGroup user
 *
 * @apiDesc  Used to Logout Users from Application.
 *
 * @apiSuccess {result}  Success Message will be sent.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {
      status: ''
    }
 *
 * @apiError Value Provided by User is Incorrect.
 *
 * @apiErrorExample Error-Response:
 *    HTTP/1.1 201 Not Found
 *    {
 *      success: 'false',
 *      message: 'Internal Server error'
 *    }
 * @apiImplementationSteps
 *   1. Delete JWT Token For User.
 *   2. IF sucessfully send succressResponse.
 *   3. IF Erorr Send Error-Response.
 */

router.get("/logout", function (req, res) {
  let allUsers = "";
  let result = "";
  const userToken = req.headers.token;
  async.series(
    [
      // Step-1 Delete JWT Token For User.
      function (callback) {
        jwt.verify(userToken, process.env.SECRET_KEY, function (err, decoded) {
          if (err) {
            // Step-3 IF Erorr Send Error-Response.
            errorMessage = {
              success: false,
              message: "User UnAuthorized Token",
            };
            res.status(HttpStatus.EXPECTATION_FAILED).send(errorMessage);
          } else {
            console.log(decoded.foo); // bar
            var refreshTokens = {};
            //here name is saved in payload of refresh token
            refreshTokens[name] = refreshToken;
            //decode the JWT Token
            var decoded = jwt.decode(decoded);
            //check in temp location
            if (refreshTokens[decoded.name] === decoded.name)
              // verify refresh token using jwt "AuthenticateJWT" function mentioned above and generate new access and refresh token for further use.
              var decoded = jwt.decode(decoded.name);
            delete refreshTokens[decoded.name];
            callback(null, 1);
          }
        });
      },
    ],
    function () {
      result = {
        status: "Success",
      };
      // Step-2 IF sucessfully send succressResponse
      res.status(HttpStatus.OK).send(result);
    }
  );
});

module.exports = router;
